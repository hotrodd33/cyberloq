<?php
session_start();
/**
 * ws_language.php
 * 11/14/17
**/

//http://cyberloqwp.com/ws_language.php?l=english
//http://68.15.33.169/webportal/ws_language.php?l=chinese

/*
$xml  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><cblresponse><languages>" . $row['xml_text'] . "</languages></cblresponse>";
*/

require_once('opendb.php');
$language = $_GET['l'];
$sql = "select convert (nvarchar(3072), (select * from [language] where language='$language' for xml path ('record') )) as [xml_text]";   
$results = sqlsrv_query($conn, $sql);

if( $results === false)  
{  
     echo "Error in query preparation/execution.\n";  
     die( print_r( sqlsrv_errors(), true));  
}  

while ($row = sqlsrv_fetch_array($results, SQLSRV_FETCH_ASSOC)) 
{
	$xml  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" . $row['xml_text'];
}

sqlsrv_free_stmt($results);
header ("Content-Type: text/xml; charset=utf-8");
echo $xml;
return $xml;
?>
