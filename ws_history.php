<?php
session_start();
/**
 * ws_history.php
 * 04/03/18
**/

//http://cyberloqwp.com/ws_history.php?u=mcarten4012861177

$username = $_GET['u'];
require_once('opendb.php');
$xml  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
$root_element1 = "history";
$xml  .= "<$root_element1>";
$sql = "select top 50 * from trailer where username='$username' order by datevisit desc";   
$results= sqlsrv_query($conn, $sql);
if( $results === false)  
{  
     echo "Error in query preparation/execution.\n";  
     die( print_r( sqlsrv_errors(), true));  
}  
while ($row = sqlsrv_fetch_array($results, SQLSRV_FETCH_ASSOC)) 
{
	$xml .= "<record>";
	
	$key = "client";
	$xml .= "<$key>";
	$xml .=$row[$key];
    $xml .= "</$key>";

	$key = "datevisit";
	$xml .= "<$key>";
    $xml .=$row[$key]->format('Y/m/d H:i:s');   
	$xml .= "</$key>";

	$key = "ipaddress";
	$xml .= "<$key>";
	$xml .=$row[$key];
    $xml .= "</$key>";

	$key = "deviceid";
	$xml .= "<$key>";
	$xml .=$row[$key];
    $xml .= "</$key>";

	$key = "accounttitle";
	$xml .= "<$key>";
	$xml .=$row[$key];
    $xml .= "</$key>";

	$key = "useremail";
	$xml .= "<$key>";
	$xml .=$row[$key];
    $xml .= "</$key>";

	$key = "usercell";
	$xml .= "<$key>";
	$xml .=$row[$key];
    $xml .= "</$key>";

	$key = "sendsms";
	$xml .= "<$key>";
	$xml .=$row[$key];
    $xml .= "</$key>";

	$key = "sendemail";
	$xml .= "<$key>";
	$xml .=$row[$key];
    $xml .= "</$key>";

	$key = "fromwhat";
	$xml .= "<$key>";
	$xml .=$row[$key];
    $xml .= "</$key>";

	$key = "processed";
	$xml .= "<$key>";
	$xml .=$row[$key];
    $xml .= "</$key>";

	$key = "appversion";
	$xml .= "<$key>";
	$xml .=$row[$key];
    $xml .= "</$key>";

	$xml .= "</record>";
}
sqlsrv_free_stmt($results);
$xml .= "</$root_element1>";
header ("Content-Type:text/xml");
echo $xml;
return $xml;
?>

   