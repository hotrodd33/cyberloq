﻿<?php
$fromwhere = $_GET['fromwhere'];
$_SESSION["fromwhere"] = $fromwhere;
$L = $_SESSION["language"];
if (isset($_POST['L'])) 
{
 $L = $_POST['L'];
} else {
 $L = 'English';
}

if ($L=='English')
{
$_SESSION["language"] = $L;
$but_login = "Login";
$but_exit = "Exit";
$but_username = "User Name";
$but_password = "Password";
$but_history = "History";
$but_devices = "Devices";
$but_activateselected = "Activate Selected";
$but_deactivateall = "Deactivate All";
}
if ($L=='French')
{
$_SESSION["language"] = $L;
$but_login = "Login";
$but_exit = "Quitter";
$but_username = "Nom d'utilisateur";
$but_password = "Mot de passe";
$but_history = "Histoire";
$but_devices = "Dispositifs";
$but_activateselected = "Activer la selection";
$but_deactivateall = "Desactiver tout";
}
if ($L=='Chinese')
{
$_SESSION["language"] = $L;
$but_login = "登錄";
$but_exit = "放棄";
$but_username = "用户名";
$but_password = "密码";
$but_history = "历史";
$but_devices = "设备";
$but_activateselected = "激活选定";
$but_deactivateall = "全部关闭";
}
if ($L=='Spanish')
{
$_SESSION["language"] = $L;
$but_login = "Login";
$but_exit = "Dejar";
$but_username = "Nombre de usuario";
$but_password = "Contraseña";
$but_history = "Historia";
$but_devices = "Dispositivos";
$but_activateselected = "Activar seleccionado";
$but_deactivateall = "Desactivar todo";
}
if ($L=='German')
{
$_SESSION["language"] = $L;
$but_login = "Anmeldung";
$but_exit = "Ausgang";
$but_username = "Nutzername";
$but_password = "Passwort";
$but_history = "Geschichte";
$but_devices = "Geräte";
$but_activateselected = "aktivieren Sie";
$but_deactivateall = "Deaktivieren";
}

//echo "exit: " . $but_exit . "<br>";
//echo "history: " . $but_history . "<br>";
//echo "devices: " . $but_devices . "<br>";
//echo "activateselected: " . $but_activateselected . "<br>";
//echo "deactivateall: " . $but_deactivateall . "<br>";

?>