﻿<?php
session_start();
header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html>
<!-- accounts.php -->
<!-- 08/05/2018 -->

<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/style.css">
<script type="text/javascript" src="scripts/scripts.js"></script>
<script>
var timeLeft = 3 * 60
function startTimer()
{
  timeLeft --
  if (timeLeft < 0) {document.location = "default.php"}
  minsLeft = Math.floor(timeLeft / 60)
  secsLeft = timeLeft - (minsLeft * 60)
  if (secsLeft < 10) {secsLeft = "0" + secsLeft}
  document.getElementById('expires').innerHTML = "Session will expire in <span style=\"color:yellow;font-weight:bold;\">" + minsLeft + ":" + secsLeft + "</span>";
  timerID  = setTimeout("startTimer()",1000)
}
</script> 
<?php 
require_once('opendb.php');
$L = $_SESSION["language"];
if ($L=='English')
{
$but_exit = "Exit";
$but_history = "History";
$but_devices = "Devices";
$but_activateselected = "Activate Selected";
$but_deactivateall = "Deactivate All";
}
if ($L=='French')
{
$but_exit = "Quitter";
$but_history = "Histoire";
$but_devices = "Dispositifs";
$but_activateselected = "Activer la selection";
$but_deactivateall = "Desactiver tout";
}
if ($L=='Chinese')
{
$but_exit = "放棄";
$but_history = "历史";
$but_devices = "设备";
$but_activateselected = "激活选定";
$but_deactivateall = "全部关闭";
}
if ($L=='Spanish')
{
$but_exit = "Dejar";
$but_history = "Historia";
$but_devices = "Dispositivos";
$but_activateselected = "Activar seleccionado";
$but_deactivateall = "Desactivar todo";
}


$fromwhere = $_GET['fw'];
//$fromwhere = $_SESSION["fromwhere"];
$username = $_SESSION["username"];
$fullname = $_SESSION["fullname"];

//echo "fromwhere: " . $fromwhere . "<br>";
//echo "username: " . $username . "<br>";
//echo "fullname: " . $fullname . "<br>";

//require_once('zoompasslogin.php');
?>

</head>
<body onLoad="startTimer()">
	
		<?php
$datasource = $_SESSION["datasource"];
$urltxt=$datasource . "ws_accounts.php?u=" . $username;
$xml = new SimpleXMLElement(file_get_contents($urltxt));
//var_dump($xml); 
//print_r($xml);



$rn=-1;
foreach($xml->record as $val)
{
  if ($val->title <> "- Click here to select -")
  {
  $rn++;
  $array_title[$rn] = $val->title;
  $array_active[$rn] = $val->active;
  $array_zoompass[$rn] = $val->zoompass;
  $array_proxy[$rn] = $val->bankcardproxy;
  //echo($rn . '  ' . $array_title[$rn] . '  ' . $array_active[$rn] . '  ' . $array_zoompass[$rn] . '  ' . $array_proxy[$rn] . "<br>");
  }
}
//echo '<br>';
//for ($xx = 0; $xx <= $rn; $xx++) {echo $xx. ' ' . $array_title[$xx] . ' ' . $array_active[$xx] . '<br>';}

if (isset($_POST["formDeactivateAll"]) && !empty($_POST["formDeactivateAll"]))
{
//check for active zoompass cards
//$sql = "select * from member_accounts where username='$username' and zoompass='Y'";
//$results= sqlsrv_query($conn, $sql);
//if( $results === false) {echo "Error in query preparation/execution.\n"; die( print_r( sqlsrv_errors(), true));} 
//while ($row = sqlsrv_fetch_array($results, SQLSRV_FETCH_ASSOC))
//{
//  $key = "bankcardproxy";
//  $proxy = $row[$key];
//  $curl = curl_init();
//  curl_setopt_array($curl, array(
//    CURLOPT_URL => "https://api.zoompass.com/pm-api/suspendCard/?proxyNumber=" . $proxy,
//    CURLOPT_RETURNTRANSFER => true,
//    CURLOPT_ENCODING => "",
//    CURLOPT_MAXREDIRS => 10,
//    CURLOPT_TIMEOUT => 30,
//    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//    CURLOPT_CUSTOMREQUEST => "GET",
//    CURLOPT_HTTPHEADER => array("authorization:" . $auth,"cache-control: no-cache")
//  ));
//  $response = curl_exec($curl);
//  $err = curl_error($curl); 
//  curl_close($curl);
//  if ($err) 
//  {
//     echo "curl error: " . $err;
//    } else {
//    if (strpos($response,"error")) 
//    {
//     //echo "card is already suspended";  
//     } else {
//     //echo "card is now suspended";  
//    }
//  }
//} 

$sql = "update member_accounts set active = 'N' where username='$username'";
$results= sqlsrv_query($conn, $sql);
if( $results === false) {echo "Error in query preparation/execution.\n"; die( print_r( sqlsrv_errors(), true));} 

$sql = "update members set online='N' where username='$username'";
$results= sqlsrv_query($conn, $sql);
if( $results === false) {echo "Error in query preparation/execution.\n"; die( print_r( sqlsrv_errors(), true));} 

header("Location: accounts.php"); 
} //end deactivate all

//activate accounts
if (isset($_POST['formSubmit'])) 
{
  $acct = $_POST['acctlist'];
  if(empty($acct)) 
 {
	//echo("<p>No accounts were selected</p>\n");
 }  else  {
  $totrec = count($acct);
  //echo("You selected $totrec accounts(s)" . '<br>');
  //require_once('zoompasslogin.php');

  for ($xx = 0; $xx < $totrec; $xx++)
	{ 
	  $zz=$acct[$xx];
	  //echo($xx . '  ' . $acct[$xx] . '  ' . $array_title[$zz] . '<br>');
	  $accounttitle = $array_title[$zz];
      $zoompass = $array_zoompass[$zz];
	  $proxy = $array_proxy[$zz];
	  updatedata();
	}
	  header("Location: accounts.php"); 
 }
}
if ($fromwhere == "menu")
				{
				$exitURL = "buttonAdmin";
				} else {
				$exitURL = "buttonExit";
				}
?>
<div id="cq_container" class="container-fluid cq_body text-center">
	<div id="expires" class="row text-center">			
	</div>
	
	<div class="row">
		<img class="img-responsive cq_logo" src="images/cyberloq_logo.png">
		<h1 class="text-center">Web Portal Activation</h1>
		<h2 class="text-center">Accounts for <?php echo $fullname; ?></h2>
	</div>
	<div class="row">
		<div class="text-center">
		
				<form name="cq_action" method="post">
					<input type='submit' value='<?php echo $but_history ?>' name='Hist' class='button' onclick="return buttonHist()">
					<input type='submit' value='<?php echo $but_devices ?>' name='Dev' class='button'onclick="return buttonDev()">
					<input type='submit' value='<?php echo $but_exit ?>' name='Exit' class='button'onclick="return <?php echo $exitURL ?>()">
				</form>				
		</div>
	</div>
	<div class="row">
		<center>
			<table class="table-striped cq_table">
				<tr>
					<td></td>
					<td><b>Account</b></td>
					<td><b>Select</b></td>
					<td></td>
				</tr>
				<form action='accounts.php' method='post'>
				<?php 
				for ($xx = 0; $xx <= $rn; $xx++) 
				{
				  $cbval = $array_active[$xx]; // Y or N 
				  $cnt=$xx+1;
				  echo "<tr>";
				  echo "<td valign='middle'><b>" . $cnt . "</b></td>";
				  echo "<td><b>" . $array_title[$xx] . "</b></td>";
				  echo "<td>";
				  echo "<label class='switch'>";
				  echo "<input type='checkbox' name='acctlist[]' value=$xx>";
				  //if ($cbval == 'Y') {echo "<input type='checkbox' checked value=cbval>";}
				  //if ($cbval == 'N') {echo "<input type='checkbox' value=cbval>";}
				  //if ($cbval == 'Y') {echo "<input type='checkbox' name='acctlist[]' checked value=cbval>";}
				  //if ($cbval == 'N') {echo "<input type='checkbox' name='acctlist[]' value=cbval>";}
				  echo "<span class='slider round'></span>";
				  echo "</label>";
				  if ($cbval=="Y") 
				  {
					$yn="On";
					echo "<td valign='middle'><b>" . $yn . "</b></td>";
				  }
				  if ($cbval=="N") 
				  {
					$yn="Off";
					echo "<td valign='middle'><b>" . $yn . "</b></td>";
				  }
				  echo "</tr>";
				} ?>
			</table>
			</center>
		<div class="row text-center">
			<div class="col-sm-4 col-sm-offset-4">
				<input type='submit' value='<?php echo $but_activateselected; ?>' id='buttonActive' class='button cq_submit_lg'>
				<input name='formSubmit' type='hidden' value='true'>
				<input type='submit' value='<?php echo $but_deactivateall; ?>' id='buttonDeact' class='button cq_submit_lg'>
				<input name='formDeactivateAll' type='hidden' value='true'>
				</form>
			</div>
		</div>			
	</div>
	<div id="version" class="row text-center">
		<h6>ver 1.08 &copy; Copyright 2018 CyberloQ</h6>
	</div>

</div>
<?php

function updatedata()
{
global $username;
global $accounttitle;
global $zoompass;
global $proxy;
global $conn;
global $auth;
global $t_cleint;
//echo "username: " . $username . "<br>";
//echo "account: " . $accounttitle . "<br>";
//echo "zoompass: " . $zoompass . "<br>";
//echo "proxy: " . $proxy . "<br>";

//If ($zoompass == "Y")
//{
  //un-suspend card
  //require_once('zoompasslogin.php');
//  $curl = curl_init();
//  curl_setopt_array($curl, array(
//  CURLOPT_URL => "https://api.zoompass.com/pm-api/unSuspendCard/?proxyNumber=" . $proxy,
//  CURLOPT_RETURNTRANSFER => true,
//  CURLOPT_ENCODING => "",
//  CURLOPT_MAXREDIRS => 10,
//  CURLOPT_TIMEOUT => 30,
//  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//  CURLOPT_CUSTOMREQUEST => "GET",
//  CURLOPT_HTTPHEADER => array("authorization:" . $auth,"cache-control: no-cache")
//));
//$response = curl_exec($curl);
//$err = curl_error($curl); 
//curl_close($curl);
//if ($err) {echo "curl error #:" . $err;}
//} 

//insert trailer and activate member
$xml  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
$sql = "select client, email, usercell, sendsms, sendemail from members where username='$username'";
$results= sqlsrv_query($conn, $sql);
if( $results === false) {echo "Error in query preparation/execution.\n"; die( print_r( sqlsrv_errors(), true));} 
while ($row = sqlsrv_fetch_array($results, SQLSRV_FETCH_ASSOC))
{
  $key = "client";
  $client = $row[$key];
  $key = "email";
  $email = $row[$key];
  $key = "usercell";
  $usercell = $row[$key];
  $key = "sendsms";
  $sendsms = $row[$key];
  $key = "sendemail";
  $sendemail = $row[$key];
}

//echo "email: " . $email . "<br>";
//echo "usercell: " . $usercell . "<br>";
//echo "sendsms: " . $sendsms . "<br>";
//echo "sendemail: " . $sendemail . "<br>";

$ip=$_SERVER['REMOTE_ADDR'];
$dt = date('Y-m-d H:i:s');
$deviceid = 'Web Portal';
$model = 'Web Portal';
echo "client: " . $t_client . "<br>";
$sql ="insert into trailer (client,deviceid,datevisit,username,useremail,usercell,sendsms,sendemail,fromwhat,accounttitle,ipaddress,processed,deactivated) 
                    values ('$client','$deviceid','$dt','$username','$email','$usercell','$sendsms','$sendemail','$model','$accounttitle','$ip','N','N')";
$results= sqlsrv_query($conn, $sql);
if( $results === false) {echo "Error in query preparation/execution.\n"; die( print_r( sqlsrv_errors(), true));}

$sql = "update member_accounts set active = 'Y' where username='$username' and title='$accounttitle'";
$results= sqlsrv_query($conn, $sql);
if( $results === false) {echo "Error in query preparation/execution.\n"; die( print_r( sqlsrv_errors(), true));}

$sql = "update members set lastlogin = '$dt', visits=visits+1, online='Y' where username='$username'";
$results= sqlsrv_query($conn, $sql);
if( $results === false) {echo "Error in query preparation/execution.\n"; die( print_r( sqlsrv_errors(), true));}

} //end updatedata
 ?>

</body>
</html>
