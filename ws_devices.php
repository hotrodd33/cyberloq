<?php
session_start();
/**
 * ws_devices.php
 * 11/12/17
**/

//https://cyberloqdb.com/webportal/ws_devices.php?u=mcarten4012861177
//http://68.15.33.169/webportal/ws_devices.php?u=mcarten4012861177

$username = $_GET['u'];
require_once('opendb.php');
$xml  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
$root_element1 = "devices";
$xml  .= "<$root_element1>";
$sql = "select top 50 * from devices where username='$username' order by deviceid desc";   
$results= sqlsrv_query($conn, $sql);
if( $results === false)  
{  
     echo "Error in query preparation/execution.\n";  
     die( print_r( sqlsrv_errors(), true));  
}  
while ($row = sqlsrv_fetch_array($results, SQLSRV_FETCH_ASSOC)) 
{
	$xml .= "<record>";

	$key = "deviceid";
	$xml .= "<$key>";
	$xml .=$row[$key];
    $xml .= "</$key>";
	
	$key = "mfg";
	$xml .= "<$key>";
	$xml .=$row[$key];
    $xml .= "</$key>";

	$key = "model";
	$xml .= "<$key>";
	$xml .=$row[$key];
    $xml .= "</$key>";

	$key = "gps";
	$xml .= "<$key>";
	$xml .=$row[$key];
    $xml .= "</$key>";

	$key = "datestart";
	$xml .= "<$key>";
    $xml .=$row[$key]->format('Y/m/d H:i:s');   
	$xml .= "</$key>";

	$xml .= "</record>";
}
sqlsrv_free_stmt($results);
$xml .= "</$root_element1>";
header ("Content-Type:text/xml");
echo $xml;
return $xml;
?>

   