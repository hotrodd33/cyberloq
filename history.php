﻿<?php
session_start();
header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html>
<!-- history.php --> 
<!-- 11/28/17 -->

<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/style.css">
<?php 
$username = $_SESSION["username"];
$fullname = $_SESSION["fullname"];
$L = $_SESSION["language"];
if ($L=='English')
{
$but_exit = "Exit";
}
if ($L=='French')
{
$but_exit = "Quitter";
}
if ($L=='Chinese')
{
$but_exit = "放棄";
}
if ($L=='Spanish')
{
$but_exit = "Dejar";
}
 ?>
</head>
<body>
<div id="cq_container" class="container-fluid cq_body text-center">
		
		<div class="row center-block">
			<img class="img-responsive cq_logo" src="images/cyberloq_logo.png">
			<h1>Activation History for <?php echo $fullname; ?></h1>			
		</div>
		<div class="row text-center">
			<form action='accounts.php' method='post'>
				<input type='submit' value='<?php echo $but_exit; ?>' id='buttonExit' class="button cq_submit">
			</form>
		</div>
		<div class="row"><center>
			<table class='table-striped cq_table' width='100%'>
				<tr>
					<td></td>
					<td><b>Client</b></td>
					<td><b>Date</b></td>
					<td><b>DeviceID</b></td>  
					<td><b>From</b></td>
					<td><b>IPaddress</b></td> 
					<td><b>Account</b></td> 
					<td><b>Version</b></td>
				</tr>

				<?php
				$datasource = $_SESSION["datasource"];
				$urltxt=$datasource . "ws_history.php?u=" . $username;
				$xml = new SimpleXMLElement(file_get_contents($urltxt));
				$rn=0;
				foreach($xml->record as $val) 
				{
				   $client = $val->client; 
				   $datevisit = $val->datevisit; 
				   $deviceid = $val->deviceid; 
				   $useremail = $val->useremail; 
				   $usercell = $val->usercell; 
				   $sendsms = $val->sendsms; 
				   $sendemail = $val->sendemail; 
				   $fromwhat = $val->fromwhat; 
				   $ipaddress = $val->ipaddress; 
				   $accounttitle = $val->accounttitle; 
				   $processed = $val->processed; 
				   $appversion = $val->appversion; 
				   $rn++;
				   echo "<tr>";
				   echo "<td valign='middle'><b>" . $rn . "</b></td>";
				   echo "<td><b>" . $client . "</b></td>";
				   echo "<td><b>" . $datevisit . "</b></td>";
				   echo "<td><b>" . $deviceid . "</b></td>";
				   echo "<td><b>" . $fromwhat . "</b></td>"; 
				   echo "<td><b>" . $ipaddress . "</b></td>"; 
				   echo "<td><b>" . $accounttitle . "</b></td>";
				   echo "<td><b>" . $appversion . "</b></td>"; 
				   echo "</tr>";
				}
				?>
			</table></center>
		</div>
		<div id="version" class="row text-center">
			<h6>ver 1.08 &copy; Copyright 2018 CyberloQ</h6>
		</div>
</div>


<?php
//echo '<pre>';
//print_r($xml);
//echo '</pre>';

?>

</body>
</html>
