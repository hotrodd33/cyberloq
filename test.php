<!doctype html>

<html lang="en">

<head>
    <meta charset="utf-8">

    <title>CyberloQ Secure Portal Login</title>
    <meta name="description" content="CyberloQ secure protal login for clients">
    <meta name="author" content="CyberloQ">

    <link rel="stylesheet" href="default.css">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
  <![endif]-->

</head>

<body>

    <div id="content" class="mobile-login">
        <div id="language-bar">
               <table id="languageTable">
            <tbody>
                <tr>
                    <td>
                        <form action="default.php" method="post">
                            <input class="blue_btn" type="submit" value="English" name="English" id="button2">
                        </form>
                    </td>
                    <td>
                        <form action="default.php" method="post">
                            <input class="blue_btn" type="submit" value="French" name="French" id="button3">
                        </form>
                    </td>
                    <td>
                        <form action="default.php" method="post">
                            <input class="blue_btn" type="submit" value="Chinese" name="Chinese" id="button4">
                        </form>
                    </td>
                    <td>
                        <form action="default.php" method="post">
                            <input class="blue_btn" type="submit" value="Spanish" name="Spanish" id="button5">
                        </form>
                    </td>
                    <td>
                        <form action="default.php" method="post">
                            <input class="blue_btn" type="submit" value="German" name="German" id="button6">
                        </form>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div id="logo-container">
        <img src="cyberloq_logo.png" width="500" height="125">
    </div>
        <h2>Web Portal Login</h2>
        <form name="form" action="login2.php" method="get">
            <table id="credentials">
                <tbody>
                    <tr>
                        <td align="right" width="200">
                            User Name
                        </td>
                        <td width="200">
                            <input type="text" size="25" name="u">
                        </td>
                    </tr>
                    <tr>
                        <td align="right" width="200">
                            Password
                        </td>
                        <td width="200">
                            <input type="password" size="25" name="p">
                        </td>
                    </tr>
                </tbody>
            </table>
            <table id="submission">
                <tbody>
                    <tr>
                        <td align="right">
                            <input class="blue_btn" type="submit" value="Login" id="button1">
                        </td>
                        <td></td>
                        <td align="left">
                            <form action="https://www.cyberloq.com" method="get">
                                <input class="blue_btn" type="submit" value="Exit" id="button7">
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
</body>

</html>
