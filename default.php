﻿<?php
session_start();
header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html>
<!-- default.php -->
<!-- 08/05/2018 -->
<!-- www.cyberloqwp.com
//http://68.15.33.169/webportal/default.php
//http://cyberloqwp.com
-->
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/style.css">
<?php 
$fromwhere = $_GET['fw'];
if (isset($_POST['L'])) 
{
 $L = $_POST['L'];
} else {
 $L = 'English';
}

if ($L=='English')
{
$_SESSION["language"] = $L;
$but_login = "Login";
$but_exit = "Exit";
$but_username = "User Name";
$but_password = "Password";
}
if ($L=='French')
{
$_SESSION["language"] = $L;
$but_login = "Login";
$but_exit = "Quitter";
$but_username = "Nom d'utilisateur";
$but_password = "Mot de passe";
}
if ($L=='Chinese')
{
$_SESSION["language"] = $L;
$but_login = "登錄";
$but_exit = "放棄";
$but_username = "用户名";
$but_password = "密码";
}
if ($L=='Spanish')
{
$_SESSION["language"] = $L;
$but_login = "Login";
$but_exit = "Dejar";
$but_username = "Nombre de usuario";
$but_password = "Contraseña";
}
if ($L=='German')
{
$_SESSION["language"] = $L;
$but_login = "Anmeldung";
$but_exit = "Ausgang";
$but_username = "Nutzername";
$but_password = "Passwort";
}; ?>
</head>
<body>
	<div id="cq_container" class="container-fluid cq_body">
		<div class="row">
			<div class="text-center">
				<form action='default.php' method='post'>
					<input type='submit' value='English' name='L' id='english' class="button">				
					<input type='submit' value='French' name='L' id='french' class="button">				
					<input type='submit' value='Chinese' name='L' id='chinese' class="button">				
					<input type='submit' value='Spanish' name='L' id='spanish' class="button">				
					<input type='submit' value='German' name='L' id='german' class="button">
				</form>
			</div>
		</div>
		<div class="row">
			<img class="img-responsive cq_logo" src="images/cyberloq_logo.png">
			<h1 class="text-center">Web Portal Login</h1>
		</div>
		<div class="row">
			<form class="form-horizontal" name="form" action="login2.php" method="get">
				<div class="form-group">
					<input type="text" class="form-control" id="inputUser" placeholder="<?php echo $but_username;?>" name="u">
				</div>
				<div class="form-group">
					<input type="password" class="form-control" id="inputPass" placeholder="<?php echo $but_password;?>" name="p">
				</div>		
				<div class="form-group row">
					<div class="text-right col-xs-6">
						<input type='submit' value="<?php echo $but_login; ?>" id='submit' class="button cq_submit_lg">
						</form>
					</div>				
					<div class="text-left col-xs-6">
						<?php
										
							if ($fromwhere == "menu")
								{
								echo "<form action='https://cyberloqdb.com/admin/adminmenu.php' method='get'>";
								} else {
								echo "<form action='default.php' method='get'>";
								}
								echo "<input type='submit' value='$but_exit' id='exit' class='button cq_submit_lg'></form>";
						?>
					</div>
				</div>			
			
		</div>
	</div>
</body>
</html>
